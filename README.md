# Dynexite Deployment Scripts

Contains docker compose scripts for a simplified and fast Dynexite installation. 

- Requires `nginx-proxy` and Let's Encrypt ACME container (see https://github.com/nginx-proxy/nginx-proxy).
- Requires `docker compose` Version v2.20 to function (e.g. Docker Engine 24).

## Instructions

Select desired version branch and follow instructions.

Typically:
- Clone git repository to location
- Copy `docker-compose.yml.sample` to `docker-compose.yml`
- Active modules by uncommenting them
- Copy matching `.env.<app>` to `.env`
- Add required information.

### Update 

- `git pull` to get newest files.
- Check if something changed in the `.env.<app>` file.
- Use `docker compose pull` to force download new images.
- `docker compose up -d` to update the application.

### Upgrade

- `git checkout <new-version>`
- `git pull` to ensure that all files are up-to-date.
- Compare `.env.<app>` files.
- Use `docker compose pull` to force download new images.
- `docker compose up -d` to update the application.
